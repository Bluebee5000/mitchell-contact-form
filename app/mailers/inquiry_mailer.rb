class InquiryMailer < ActionMailer
  # give default destination inbox
  default to: 'hiremitchellhoffarth@gmail.com'
  
  # this is the email that gets sent after the inquiry form data is saved to your db
  # inquiry_email is the name of the inquiry_mailer view file inquiry_email.html.erb
  def inquiry_email(name, email, subject, body)
    # define pieces of inquiry data as instance variables
    @one = one
    @two = two
    @three = three
    @four = four
    @five = five
    @six = six
    @seven = seven
    @eight = eight
    @nine = nine
    @ten = ten
    
    # displays user data from the inquiry form in the email they send to you
    mail(from: @email, subject: @subject)
  end
  
end