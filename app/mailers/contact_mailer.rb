class ContactMailer < ActionMailer::Base
  # give default destination inbox
  default to: 'hiremitchellhoffarth@gmail.com'
  
  # this is the email that gets sent after the contact form data is saved to your db
  # contact_email is the name of the contact_mailer view file contact_email.html.erb
  def contact_email(name, email, subject, body)
    # define pieces of contact data as instance variables
    @name = name
    @email = email
    @subject = subject
    @message = body
    
    # displays user data from the contact form in the email they send to you
    mail(from: @email, subject: @subject)
  end
end