class Contact < ActiveRecord::Base
  # check for certain info before contact.save
  validates :name, presence: true
  validates :email, presence: true
  validates :subject, presence: true
  validates :message, presence: true
end