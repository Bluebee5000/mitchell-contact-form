class InquiriesController < ApplicationController
  def new
    # Creates blank inquiry obejct on sever memory
    @inquiry = Inquiry.new
  end
  
  def create
    @inquiry = Inquiry.new(inquiry_params)
    
    # If form is filled out correctly
    if @inquiry.save
      # [:inquiry] is a class name used in custom.css.scss
      flash[:inquiry] = "Inquiry Submitted Successfully"
      redirect_to root_path
    else # otherwise take them to the blank contact form again with error message
      # [:inquiryfail] is a class name used in custom.css.scss
      flash[:inquiryfail] = "Inquiry Submission failed. Please Fill in all fields."
      redirect_to new_inquiry_path # because the inquires form is at that address
    end
  end
  
  private
  # Whitelist information I am willing to receive from the outside world
    def inquiry_params                # List of columns from the inquiries db table
      params.require(:inquiry).permit(:one, :two, :three, :four, :five, :six, :seven, :eight, :nine, :ten)
    end
end