class ContactsController < ApplicationController
  def new
    # Creates blank contact obejct on sever memory
    @contact = Contact.new
  end
  
  def create
    @contact = Contact.new(contact_params)
    
    # If form is filled out correctly
    if @contact.save
      # assigning variables to contacts db tables columns
      name = params[:contact][:name]
      email = params[:contact][:email]
      subject = params[:contact][:subject]
      body = params[:contact][:message]
      
      # call the contact_mailer file and run the contact_email method with its arguments (name, email, subject, body)
      ContactMailer.contact_email(name, email, subject, body).deliver
      
      # [:notice] is a class name used in custom.css.scss
      flash[:notice] = "Message Sent Successfully"
      redirect_to root_path
    else # otherwise take them to the blank contact form again with error message
      # [:noticefail] is a class name used in custom.css.scss
      flash[:noticefail] = "Message failed to send. Please Fill out all fields."
      redirect_to new_contact_path
    end
  end
  
  private
  # Whitelist information I am willing to receive from the outside world
    def contact_params          # whitelisting the columns of the contacts db table
      params.require(:contact).permit(:name, :email, :subject, :message)
    end
end